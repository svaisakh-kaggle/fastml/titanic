import feather, subprocess

from fastai.imports import *
from fastai.structured import *

from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from pathlib import Path
from contextlib import contextmanager

shrun = lambda cmd: print(subprocess.getoutput(cmd))

def _make(self):
    self.mkdir(parents=True, exist_ok=True)
    return self
Path.make = _make

COMPETITION = 'titanic'
DIR_DATA = (Path('~/.kaggle/Competitions').expanduser() / COMPETITION).make()
DIR_CHECKPOINTS = (Path.cwd() / 'Checkpoints').make()

@contextmanager
def workdir(path):
    path_cwd = os.getcwd()
    os.chdir(path)

    yield

    os.chdir(path_cwd)

def download_and_unzip(train=True):
    filename = 'train.csv' if train else 'test.csv'

    if (DIR_DATA / filename).exists(): return

    with workdir(DIR_DATA):
        shrun(f'kaggle competitions download -c {COMPETITION} -f {filename}')

def split_val(*args, frac):
    assert np.std([len(a) for a in args]) == 0

    n_train = int(len(args[0]) * (1- frac))
    return [a[:n_train] for a in args], [a[n_train:] for a in args]

def _print_scores(model, data):
    (x, y), (x_val, y_val) = data

    print(f'Training: {model.score(x, y) * 100:.3f} %')
    print(f'Validation: {model.score(x_val, y_val) * 100:.3f} %')
    if hasattr(model, 'oob_score_'): print('oob:', model.oob_score_)

def train_model(model, data):
    x, y = data[0]

    model.fit(x, y)
    _print_scores(model, data)
    return model

def submit(x, y, message, filename='Submission'):
    filename += '.csv'

    submission = pd.DataFrame({'PassengerId': x.PassengerId, 'Survived': y})

    with workdir(DIR_CHECKPOINTS):
        submission.to_csv(filename, index=False)

        shrun(f'gzip {filename}')
        shrun(f'kaggle competitions submit {COMPETITION} -f {filename}.gz -m "{message}"')
        shrun(f'rm {filename}.gz')